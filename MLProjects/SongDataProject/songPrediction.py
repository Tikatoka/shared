import tensorflow as tf
import numpy as np
from tensorflow import keras
import time
from sklearn.ensemble import RandomForestRegressor
import matplotlib as mtl

mtl.use("TkAgg")
import matplotlib.pyplot as plt
import graphviz
from sklearn import tree


def load_data():
    print("Start loading data from file")
    start = time.time()
    data_set = []
    label_set = []
    with open("YearPredictionMSD.txt") as f:
        for line in f:
            infor_array = line.split(",")
            # label = 1 if int(infor_array[0])>2005 else 2 if int(infor_array[0])>2000 else 0
            # label = 1 if int(infor_array[0])>2005 else 2 if int(infor_array[0])>2000 else 0
            label = int(infor_array[0]) - 1922
            data = list(map(float, infor_array[1:]))
            data_set.append(data)
            label_set.append(label)
    print("All data loaded in", round(time.time() - start, 2), "s")
    return np.array(data_set), label_set


def reconstructData(data, label):
    new_data_struc = np.zeros((90, 90, 3))
    for i in range(len(label)):
        year = label[i]
        new_data_struc[year, :, 0] += 1
        new_data_struc[year, :, 1] += np.abs(data[i, :])

    for i in range(90):
        new_data_struc[i, :, 2] = new_data_struc[i, :, 1] / new_data_struc[i, :, 0]
    return new_data_struc

def plot_data(data_set, label_set):
    new_data_struc = reconstructData(data_set, label_set)
    plt.subplot(331)
    plt.plot([i for i in range(90)], new_data_struc[:, 0:12, 2])
    plt.title("Data set 1~12: the average")
    plt.subplot(332)
    plt.plot([i for i in range(90)], new_data_struc[:, 12:22, 2])
    plt.title("Data set 12~22: the variance")
    plt.subplot(333)
    plt.plot([i for i in range(90)], new_data_struc[:, 22:32, 2])
    plt.title("Data set 22~32: the variance")
    plt.subplot(334)
    plt.plot([i for i in range(90)], new_data_struc[:, 32:42, 2])
    plt.title("Data set 32~42: the variance")
    plt.subplot(335)
    plt.plot([i for i in range(90)], new_data_struc[:, 42:52, 2])
    plt.title("Data set 42~52: the variance")
    plt.subplot(336)
    plt.plot([i for i in range(90)], new_data_struc[:, 52:62, 2])
    plt.title("Data set 52~62: the variance")
    plt.subplot(337)
    plt.plot([i for i in range(90)], new_data_struc[:, 62:72, 2])
    plt.title("Data set 62~27: the variance")
    plt.subplot(338)
    plt.plot([i for i in range(90)], new_data_struc[:, 72:82, 2])
    plt.title("Data set 72~82: the variance")
    plt.subplot(339)
    plt.plot([i for i in range(90)], new_data_struc[:, 82:, 2])
    plt.title("Data set 82~90: the variance")



def train():

    # Plot out the data
    data_set, label_set = load_data()
    # data_set = data_set
    plot_data(data_set,label_set)
    plt.show()

    # Random forest regressor to select features
    # data_set = data_set[:,[1,2,3]]
    # names = [i for i in range(1,91)]
    # rf = RandomForestRegressor()
    # rf.fit(data_set,label_set)
    # with open("gini_importance", "w+") as f:
    #     for key in sorted(zip(map(lambda x: round(x, 4), rf.feature_importances_), names),
    #        reverse=True):
    #         f.write(str(key))


    # Fit the NN model considering the gini coefficient
    # with open("gini_importance") as f:
    #     line = f.readline()
    # datas = line.split(")(")
    # key = []
    # for i in datas:
    #     key.append(int(i.split(",")[1]))
    # data_set = data_set[:, key[70:]]

    # NN model for the fit and predict
    # model = keras.Sequential([
    #     keras.layers.Flatten(input_shape=(20,)),
    #     # keras.layers.Dense(128, activation=tf.nn.sigmoid),
    #     # keras.layers.Dense(80, activation=tf.nn.sigmoid),
    #     # keras.layers.Dense(64, activation=tf.nn.sigmoid),
    #     keras.layers.Dense(90, activation=tf.nn.sigmoid)
    # ])
    # model.compile(optimizer=tf.train.AdamOptimizer(),
    #               loss='sparse_categorical_crossentropy',
    #               metrics=['accuracy'])
    # model.fit(data_set, label_set, epochs=5)
    # plt.hist(label_set, bins=30)
    # plt.show()


    # Decision tree approach
    # clf = tree.DecisionTreeClassifier()
    #
    # clf.fit(data_set, label_set)
    # dot_data = tree.export_graphviz(clf, out_file=None)
    # clf.predict(data_set)
    # graph = graphviz.Source(dot_data)
    # graph.render("iris")


if __name__ == "__main__":
    train()
